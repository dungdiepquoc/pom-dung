Feature: Add Customer in system
  In order to access the system
  I want to have the adding Customer in system

  Scenario Outline: Add customer successfully
    Given I open the website <website>
    Then I verify that the correct Add Customer form
    When I type firstname <firstname>, last name <lastname>, poster code <postercode>
    And I click Add Cutomer button
    Then I see the successful dialog is appeared
    And I verify the customer information inserts successfully
    Examples:
      | website                                                                        | firstname | lastname | postercode |
      | http://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager/addCust | Hung      | Truong   | 50000      |
      | http://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager/addCust | Linh      | Hoang    | 00888      |